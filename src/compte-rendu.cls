\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{compte-rendu}[2021/10/04]
\LoadClass[a4paper]{article}

\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[french]{babel}
\RequirePackage{geometry}
\newgeometry{top=70pt, bottom=100pt, left=70pt, right=70pt}

\RequirePackage{indentfirst}

% Commande utile pour sauter des lignes facilement
\newcommand{\saut}[1][1]{\vspace{#1\baselineskip}}

\RequirePackage[ddmmyyyy]{datetime}
\RequirePackage{graphicx}
\RequirePackage{tikz}

\def\@title{Titre}

% Définition de variables utiles pour le logo
\def\@logopath{}
\def\@logoscale{1.0}

% Permet de définir le logo à utiliser dans le maketitle
\newcommand{\logo}[2][1.0]{
	\def\@logoscale{#1}	
	\def\@logopath{#2}
}

% Création du maketitle qui a besoin au moins besoin d'un titre pour compiler
% On peut définir en plus un auteur, une date, et un logo
\RequirePackage{ifthen}
\renewcommand{\maketitle}[1][]{		
	\ifthenelse{\equal{\@logopath}{}}{}{
	\begin{tikzpicture}[remember picture, overlay]
	\begin{scope}[x={(current page.north east)},y={(current page.south west)}]
	\node[inner sep=0pt] at (0.82,0.15){\includegraphics[scale=\@logoscale]{\@logopath}};
	\end{scope}	
	\end{tikzpicture}}
	
	\vspace*{15pt}
	\begin{center}
	{\huge \@title}
	\saut[0.5]
	
	{\LARGE \@author}
	\ifthenelse{\equal{\@author}{}}{}{
		\saut[0.3]
	}
	
	{\large \@date}
	\ifthenelse{\equal{\@date}{}}{}{
		\saut[0.7]
	}
	\end{center}
}

% Redéfinition des notes de bas de page
\RequirePackage{perpage}
\newcounter{footnotecounter}
\MakePerPage{footnotecounter}
\setcounter{footnotecounter}{0}
\renewcommand{\footnotemark}{\stepcounter{footnotecounter}\!$^{\thefootnotecounter}$\!}
\newcommand{\@footnote}[1]{
	\footnotemark
	\footnotetext[\thefootnotecounter]{#1}
}
\renewcommand{\footnote}[1]{\protect\@footnote{#1}}

% Définition des hauts et bas de pages
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}

\fancypagestyle{plain}{%
	\fancyhead[L]{}
	\fancyhead[C]{\@title}
	\fancyhead[R]{\@date}

  	\fancyfoot[C]{Page \thepage\ de \pageref{LastPage}}%
  	\renewcommand{\headrulewidth}{0.4pt}
  	\renewcommand{\footrulewidth}{0.4pt}
}

\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\fancyhead[L]{}
\fancyhead[C]{\@title}
\fancyhead[R]{\@date}
\fancyfoot[L, R]{}
\fancyfoot[C]{Page \thepage\ de \pageref{LastPage}}

\pagestyle{fancy}

% Permet de définir les membres présents lors d'une réunion
\newcounter{grouplevel}
\newcounter{indent}
\newcommand{\@indent}{\foreach \n in {0,...,\thegrouplevel}{\quad}}
\newcommand{\@groupe}[1]{{\huge
	\saut[0.3]
	\newline \null
	\saut[0.2]
	\textbf{#1~:} \null
	\setcounter{grouplevel}{1}
}}
\newcommand{\@sousgroupe}[1]{{\LARGE
	\ifnum\value{grouplevel}=0
		\ClassError{
			Commande interdite détectée !\MessageBreak
			Impossible de débuter un sous-groupe avant un groupe !
		}
	\fi
	\setcounter{grouplevel}{1}
	\saut[0.3]
	\newline \null
	\saut[0.2]
	\@indent
	\textbf{#1~:} \null
	\setcounter{grouplevel}{3}
}}
\newcommand{\@firstaddpresent}[1]{
	\ifnum\value{grouplevel}=3
		#1 \hspace{-10pt}
	\else
		\newline \saut[0.3] \null
		\@indent
		$\bullet$ \ #1
	\fi
	\let\@addpresent\@nextaddpresent
}
\newcommand{\@nextaddpresent}[1]{
	\ifnum\value{grouplevel}=3
		,\ #1 \hspace{-10pt}
	\else
		\newline \saut[0.3] \null
		\@indent
		$\bullet$ \ #1
	\fi
}

\NewDocumentCommand{\@present}{>{\SplitList{,}}m}{
	{\large
	\let\@addpresent\@firstaddpresent
	\hspace{-15pt} \ProcessList{#1}{\@addpresent}}
}

\newcommand{\groupe}{
	\ClassError{compte-rendu}{
		Commande interdite détectée !\MessageBreak
		La commande \protect\groupe\space n'est utilisable que dans l'environnement "presents".
	}
}
\newcommand{\sousgroupe}{
	\ClassError{compte-rendu}{
		Commande interdite détectée !\MessageBreak
		La commande \protect\sousgroupe\space n'est utilisable que dans l'environnement "presents".
	}
}
\newcommand{\present}{
	\ClassError{compte-rendu}{
		Commande interdite détectée !\MessageBreak
		La commande \protect\present\space n'est utilisable que dans l'environnement "presents".
	}
}

\newenvironment{presents}{
	\begingroup
	\noindent
	\let\groupe\@groupe
	\let\sousgroupe\@sousgroupe
	\let\present\@present
}{
	\endgroup
}

% Ajout d'une commande pour formatter rapidement les votes
\NewDocumentCommand{\vote}{m m O{"pour"} m O{"contre"} m}{
	Résultats du vote : #1 votants / \ifthenelse{\equal{#2}{0}}{personne}{#2} \ifthenelse{\equal{#2}{0}}{n'}{}\ifthenelse{#2<2}{a}{ont} voté #3 / \ifthenelse{\equal{#4}{0}}{personne}{#4} \ifthenelse{\equal{#4}{0}}{n'}{}\ifthenelse{#4<2}{a}{ont} voté #5 / #6 abstention\ifthenelse{#6>1}{s}{}.
}

\AddToHook{begindocument/end}{
	\maketitle
	\saut[2]
}